import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    {
        path:'/',
        component: require('../components/pages/CodeList').default,
        name:'codelist',
        meta: {
            title: 'Unique Codes',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/couponcode/',
        component: require('../components/pages/CreateCode').default,
        name:'couponcode',
        meta: {
            title: 'Create Coupon Code',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/apperance',
        component: require('../components/pages/Apperance').default,
        name:'apperance',
        meta: {
            title: 'Appearance',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
];


// This callback runs before every route change, including on page load.


const router = new VueRouter({
    mode:'history',
    routes,
    scrollBehavior() {
        return {
            x: 0,
            y: 0,
        };
    },

});

export default router;
