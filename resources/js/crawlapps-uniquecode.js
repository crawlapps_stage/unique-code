const host = process.env.MIX_APP_URL;
const apiEndPoint = host + '/api';

window.Vue = require('vue');

import axios from "axios";

const app = new Vue({
    template: '<div></div>',
    data: {
        shop_data: {},
    },
    methods: {
        init() {
            this.shop_data = JSON.parse(document.getElementById('crawlapps-uniquecode').innerHTML);
            if (typeof window.jQuery == 'undefined') {
                var script = document.createElement('script');
                script.type = "text/javascript";
                script.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js";
                document.getElementsByTagName('head')[0].appendChild(script);
            }
            this.cartEnableDisable(true);
            this.checkCode();
        },
        async checkCode(){
            let self = this;
            var pageURL = window.location.href;
            if(pageURL.indexOf('/products/') > -1){
                let aPIEndPoint = `${apiEndPoint}/check-code`;
                await axios({
                    url: aPIEndPoint,
                    data: {
                        'data': self.shop_data,
                    },
                    method: 'post',
                }).then(res => {
                    let data = res.data.data;
                    $('#unique-code').remove();
                    if( data.hide ){
                        self.addForm(data.display.form, data.display.css, true);
                        $('.validate_code').click(function (){
                            self.validateCode(data.data);
                        });
                    }else{
                        this.cartEnableDisable(false);
                    }
                })
                    .catch(err => {
                        console.log(err);
                    })
            }
        },
        cartEnableDisable(action){
            console.log(action);
            let forms = document.getElementsByTagName('form');
            let ac = 'https://' + window.location.hostname;
            for (var i = 0; i < forms.length; i++) {
                let str = forms[i].action.replace(ac, '');
                if (str.includes("cart")) {
                    forms[i].setAttribute("id", "cartform");
                    let atc_btn = document.getElementsByName('add');

                    if( action ){
                        atc_btn[0].setAttribute("disabled", 'disable');
                    }else{
                        atc_btn[0].removeAttribute("disabled");
                    }

                    // $(atc_btn).attr('disabled', action);
                }
            }
        },
        addForm(form, css, disable){
            let code = $('.unique-code-input').val();
            $('#unique-code').remove();
            let forms = document.getElementsByTagName('form');
            let ac = 'https://' + window.location.hostname;
            for (var i = 0; i < forms.length; i++) {
                let str = forms[i].action.replace(ac, '');
                if (str.includes("cart")) {
                    forms[i].setAttribute("id", "cartform");
                    let atc_btn = document.getElementsByName('add');
                    $(form).insertBefore(atc_btn);
                    $(atc_btn).attr('disabled', disable);
                    this.addCSS(css);

                    if( !disable ){
                        let is_hastID = document.getElementById("unique-code-prop");

                        if (!is_hastID) {
                            var div = document.createElement("div");
                            div.className = 'cart-property__field';


                            // Create a tid input
                            var hiddentid = document.createElement("input");
                            hiddentid.setAttribute("type", "hidden");
                            hiddentid.setAttribute("id", "unique-code-prop");
                            hiddentid.setAttribute("name", "properties[Unique code]"); // You may want to change this
                            hiddentid.setAttribute("value", code); // You may want to change this
                            div.appendChild(hiddentid);
                            //Append the div to the container div
                            document.getElementById("cartform").appendChild(div);
                        }
                    }
                }
            }
        },
        addCSS($style){
            var css = $style,
                head = document.head || document.getElementsByTagName('head')[0],
                style = document.createElement('style');

            head.appendChild(style);
            style.type = 'text/css';
            if (style.styleSheet) {
                // This is required for IE8 and below.
                style.styleSheet.cssText = css;
            } else {
                style.class= 'unique-code';
                style.appendChild(document.createTextNode(css));
            }
        },
        async validateCode(ids){
            let self = this;
            let code = $('.unique-code-input').val();
            if( code == '' ){

            }else{
                let aPIEndPoint = `${apiEndPoint}/validate-code`;
                await axios({
                    url: aPIEndPoint,
                    data: {
                        'data': { domain : self.shop_data.shop.domain, ids: ids, code: code},
                    },
                    method: 'post',
                }).then(res => {
                    let data = res.data.data;
                    if( data.success ){
                        if( data.msg == 'limit' ){
                            self.addForm(data.display.msg, data.display.css, true);
                            $('.ta-lnk').click(function (){
                                self.checkCode();
                            });
                        }else{
                            self.addForm(data.display.msg, data.display.css, false);
                        }
                    }else{
                        self.addForm(data.display.msg, data.display.css, true);
                        $('.ta-lnk').click(function (){
                           self.checkCode();
                        });
                    }
                })
                    .catch(err => {
                        console.log(err);
                    })
            }
        },
    },
    created() {
        this.init();
    },
});

Window.crawlapps_uniquecode = {
    init: function () {
        app.$mount();
    },
};

window.onload = Window.crawlapps_uniquecode.init();

