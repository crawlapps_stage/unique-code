<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('title')->nullable();
            $table->boolean('is_random')->default(1);
            $table->string('code_details')->nullable();
            $table->longText('generated_codes')->nullable();
            $table->string('applies_to')->nullable();
            $table->longText('apply_data')->nullable();
            $table->integer('customer_eligibility')->nullable()->comment(' 0 = all customer, 1 = specific customer');
            $table->boolean('is_enable_usage_limit')->nullable();
            $table->integer('usage_limit')->nullable();
            $table->integer('used_limit')->nullable();
            $table->boolean('is_set_end_date')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->boolean('status')->nullable()->default(1);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codes');
    }
}
