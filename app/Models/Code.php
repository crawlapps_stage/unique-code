<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $fillable = ['used_limit'];
    public function hasManyResource(){
        return $this->hasMany(Resource::class, 'code_id', 'id' );
    }
}
