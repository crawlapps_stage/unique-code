<?php namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Contracts\Objects\Values\ShopDomain;
use stdClass;

class OrdersUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param  string  $shopDomain  The shop's myshopify domain
     * @param  stdClass  $data  The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger('================== START:: Order Update Webhook ====================');
        try {
            $domain = $this->shopDomain->toNative();
            $shop = User::where('name', $domain)->first();

            $code = [];

            $order = $this->data;
            if( !empty( $order->line_items ) ) {
                foreach ($order->line_items as $lkey => $lval) {
                    if( is_array($lval->properties) && !empty($lval->properties) ) {
                        foreach ($lval->properties as $pkey => $pval) {
                            if( $pval->name == 'Unique code'){
                                $code = $pval->value;
                            };
                        }
                    }
                }
            }

            if (!empty($code)) {
                $tags = explode(',', $order->tags);
                $tags[] = $code;
                $data = [
                    "order" => [
                        "id" => $order->id,
                        "tags" => implode(',', $tags),
                    ]
                ];
                $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/orders/'. $order->id .'.json';
                $sh_order = $shop->api()->rest('PUT', $endPoint, $data);
            }
            logger('================== END:: Order Update Webhook ====================');
        } catch (\Exception $e) {
            logger('================== ERROR:: Order Update Webhook ====================');
            logger($e);
        }
    }

}
