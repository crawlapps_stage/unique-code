<?php
namespace App\Jobs;

use App\Models\Setting;
use App\Traits\Snippet;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AfterAuthentication implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use Snippet;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger('-----------------------AFTER AUTHENTICATION JOB-----------------------');
        try {
            $shop = \Auth::user();
            $style = '{"fail": {"msg": "Invalid verification code", "bg_color": "#FFD8D8", "text_color": "#B74646", "border_color": "#F7AFAF", "ta_link_color": "#0F6398"}, "display": {"title": "Enter Verification Code", "submit_btn_text": "CHECK", "input_plhol_text": "enter verification code", "submit_btn_color": "#000000", "title_text_color": "#000000", "blocked_atc_btn_text": "MUST ENTER CODE TO PURCHASE", "blocked_atc_btn_color": "#FFFFFF", "submit_btn_text_color": "#FFFFFF", "blocked_atc_btn_text_color": "#000000", "blocked_atc_btn_border_color": "#000000"}, "success": {"msg": "Success! You may now purchase this product.", "bg_color": "#D8F9D6", "text_color": "#0A792A", "border_color": "#D8F9D6"}}';

            $entity = Setting::where('user_id', $shop->id)->first();
            if (empty($entity)) {
                $entity = new Setting;
                $entity->user_id = $shop->id;
                $entity->style = $style;
                $entity->save();
            }

            $this->addSnippet($shop->name);
        }catch (\Exception $e) {
            logger('-----------------------ERROR :: AFTER AUTHENTICATION JOB-----------------------');
            logger(json_encode($e));
        }
    }
}
