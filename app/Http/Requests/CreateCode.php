<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;

class CreateCode extends FormRequest
{
    public static $rules = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Self::$rules;
        $data = $this::all();
        $data = $data['data'];
        switch (Route::currentRouteName()) {
            case 'code.store':
            {
                $rules['data.title'] = 'required';
                if( $data['is_random'] == 1){
                    $rules['data.code_details.total_codes'] = 'integer|min:1';
                    $rules['data.code_details.code_length'] = 'integer|min:1';

                    $prelen = ( $data['code_details']['prefix'] ) ? strlen($data['code_details']['prefix']) : 0;
                    $suflen = ( $data['code_details']['suffix'] ) ? strlen($data['code_details']['suffix']) : 0;
                    $presuflen = $prelen + $suflen;

                    $presuflen = ( $data['code_details']['is_include_saperater'] ) ? $presuflen + 1 : $presuflen;

                    if( $presuflen > $data['code_details']['code_length'] ){
                        $rules['data.code_details.code_length'] = 'integer|min:'.$presuflen;
                    }
                }else{
                    $rules['data.generated_code'] = 'required';
//                    foreach ( $data['generated_code'] as $key=>$val ){
//                        $rules['data.generated_code.' . $key] = 'required';
//                    }
                }
                if( $data['applies_to'] == 'specific_product'){
                    $rules['data.specific_product'] = 'required|array|min:1';
                }elseif ( $data['applies_to'] == 'specific_collection' ) $rules['data.specific_collection'] = 'required|array|min:1';
                elseif ($data['applies_to'] == 'specific_types') foreach ($data['specific_types'] as $key=> $val ){
                    $rules['data.specific_types.' . $key] = 'required';
                }
                elseif ($data['applies_to'] == 'specific_tags'){
                    foreach ( $data['specific_tags'] as $key=>$val ){
                        $rules['data.specific_tags.' . $key] = 'required';
                    }
                }
                return $rules;
            }
            case 'code.update':
            {
                $rules['data.title'] = 'required';
                if( $data['applies_to'] == 'specific_product')
                    $rules['data.specific_product'] = 'required|array|min:1';
                elseif ( $data['applies_to'] == 'specific_collection' )
                    $rules['data.specific_collection'] = 'required|array|min:1';
                elseif ($data['applies_to'] == 'specific_types') foreach ($data['specific_types'] as $key=> $val ) $rules['data.specific_types.' . $key] = 'required';
                elseif ($data['applies_to'] == 'specific_tags') foreach ($data['specific_tags'] as $key=> $val ) $rules['data.specific_tags.' . $key] = 'required';
                return $rules;
            }
            default:
                break;
        }
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        $rules = [];
        $data = $this::all();
        $data = $data['data'];
        $rules['data.title.*'] = 'Code title is required';
        if( $data['id'] == '' ){
            if( $data['is_random'] == 1){
                $rules['data.code_details.total_codes.*'] = 'Contains only positive value';
                $rules['data.code_details.code_length.integer'] = 'Contains only positive value';

                $prelen = ( $data['code_details']['prefix'] ) ? strlen($data['code_details']['prefix']) : 0;
                $suflen = ( $data['code_details']['suffix'] ) ? strlen($data['code_details']['suffix']) : 0;
                $presuflen = $prelen + $suflen;

                $presuflen = ( $data['code_details']['is_include_saperater'] ) ? $presuflen + 1 : $presuflen;


                if( $presuflen > $data['code_details']['code_length'] ){
                    $rules['data.code_details.code_length.*'] = 'Code length must be greater than length of prefix and suffix.';
                }
            }else{
                $rules['data.generated_code.*'] = 'required';
            }
        }else{
            $rules['data.generated_code.*'] = 'required';
        }

        if( $data['applies_to'] == 'specific_product'){
            $rules['data.specific_product.*'] = 'Select atleast one product';
        }elseif ( $data['applies_to'] == 'specific_collection' ){
            $rules['data.specific_collection.*'] = 'Select atleast one collection';
        }elseif ($data['applies_to'] == 'specific_types'){
            foreach ( $data['specific_types'] as $key=>$val ){
                $rules['data.specific_types.' . $key . '.*'] = 'required';
            }
        }elseif ($data['applies_to'] == 'specific_tags'){
            foreach ( $data['specific_tags'] as $key=>$val ){
                $rules['data.specific_tags.' . $key . '.*'] = 'required';
            }
        }
        return $rules;
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag);
    }
}
