<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index(){
        try{
            $shop = \Auth::user();
            $parameter['fields'] = 'id,note_attributes,tags';

            $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/orders/2730399137952.json';
            $sh_order = $shop->api()->rest('GET', $endPoint, $parameter);
            $code = [];
            $order = $sh_order['body']->container['order'];
            if( !empty( $sh_order->line_items ) ) {
                foreach ($sh_order->line_items as $lkey => $lval) {
                    if( is_array($lval->properties) && !empty($lval->properties) ) {
                        foreach ($lval->properties as $pkey => $pval) {
                            if( $pval->name == 'Unique code'){
                                $code = $pval->value;
                            };
                        }
                    }
                }
            }

            if( !empty($code)){
                $tags = explode( ',', $order['tags'] );
                $tags[] = $code;
                $data = [
                    "order"=> [
                        "id"=> 450789469,
                        "tags"=> implode( ',', $tags ),
                    ]
                ];
                $sh_order = $shop->api()->rest('PUT', $endPoint, $data);
            }
        }catch( \Exception $e ){
            dd($e);
        }
    }
}
