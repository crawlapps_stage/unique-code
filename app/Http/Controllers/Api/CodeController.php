<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Code;
use App\Models\Resource;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use Response;

class CodeController extends Controller
{
    public function index(Request $request){
        try{
            $data = $request->data;
            $res['hide'] = false;
            if( !empty($data) ){
                $template = $data['template'];
                $domain = $data['shop']['domain'];
                $shop = User::where('name', $domain)->first();
                $today = date('Y-m-d H:i:s');
                $code = Code::with('hasManyResource')->where('user_id', $shop->id)->whereDate('start_date','<=', $today)->where('status', 1)->get()->toArray();

                $templatedata = (@$data[$template]) ? $data[$template] : [];
                foreach ( $code as $key=>$val){
                    if( $val['is_set_end_date'] == 0 || ($val['is_set_end_date'] == 1 && $val['end_date'] >= $today )){
                        $apply = $val['applies_to'];
                        if( $apply == 'all_product' ){
                            $res['hide'] = true;
                            $res['data'][] = $val['id'];
                        }else {
                            $applyData = ($apply == 'specific_product' || $apply == 'specific_collection') ? $val['has_many_resource'] : json_decode($val['apply_data']);
                            if ( $apply == 'specific_product'){
                                if( !empty($data) ){
                                    foreach ( $applyData as $akey=>$aval ){
                                        if( $aval['resource_id'] == $templatedata['id'] ){
                                            $res['hide'] = true;
                                            $res['data'][] = $val['id'];
                                        }
                                    }
                                }
                            }elseif ( $apply == 'specific_collection' ){
                                $collectionData = $data['collection'];
                                foreach ( $collectionData as $ckey=>$kval ){
                                     $cids[] = $kval['id'];
                                }
                                if( !empty($data) ){
                                    foreach ( $applyData as $akey=>$aval ){
                                        if( in_array( $aval['resource_id'], $cids ) ){
                                            $res['hide'] = true;
                                            $res['data'][] = $val['id'];
                                        }
                                    }
                                }
                            }elseif ( $apply == 'specific_types' ){
                                $ttype = ( @$templatedata['type'] ) ? @$templatedata['type'] : '';
                                if( in_array($ttype, $applyData) ){
                                    $res['hide'] = true;
                                    $res['data'][] = $val['id'];
                                }
                            }elseif ( $apply == 'specific_tags' ){
                                $ttags = ( @$templatedata['tags'] ) ? @$templatedata['tags'] : [];
                                foreach ( $ttags as $tk=>$tv ){
                                    if( in_array($tv, $applyData) ){
                                        $res['hide'] = true;
                                        $res['data'][] = $val['id'];
                                    }
                                }
                            }
                        }
                    }
                }

                if( $res['hide'] ){
                    $setting = Setting::select('style')->where('user_id', $shop->id)->first();
                    $res['display'] = $this->getHtml(json_decode($setting->style), 'form', '');
                }
            }

            return response::json(['data' => $res], 200);
        }catch ( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    public function getHtml($style, $type, $msg){
        if( $type == 'form' ){
            $style= $style->display;
            $disp['form'] = "<div id='unique-code'><p class='code-title'>$style->title</p><div class='d-flex justify-content-between unique-form'><input type='text' class='unique-code-input' placeholder='".$style->input_plhol_text. "' name='unique_code' required><button type='button' class='btn btn-unique-code validate_code'>$style->submit_btn_text</button></div><button type='button' class='btn btn-blocked-unique-code'>$style->blocked_atc_btn_text</button></div>";

            $disp['css'] = "#unique-code{width: 100%;}.code-title{ margin-bottom: 5px; }.unique-code-input{width: 80%;margin-right: 10px;}.code-title{color:$style->title_text_color}.unique-form{display: flex}.btn-unique-code{color:$style->submit_btn_text_color;background-color:$style->submit_btn_color}.btn-blocked-unique-code{color:$style->blocked_atc_btn_text_color;background-color:$style->blocked_atc_btn_color;border: 1px solid $style->blocked_atc_btn_border_color;margin: 10px 0px;width: 100%;cursor: inherit;}";
            $disp['css'] = "#unique-code{width: 100%;}.code-title{ margin-bottom: 5px; }.unique-code-input{width: 80%;margin-right: 10px;}.code-title{color:$style->title_text_color}.unique-form{display: flex}.btn-unique-code{color:$style->submit_btn_text_color;background-color:$style->submit_btn_color}.btn-blocked-unique-code{color:$style->blocked_atc_btn_text_color;background-color:$style->blocked_atc_btn_color;border: 1px solid $style->blocked_atc_btn_border_color;margin: 10px 0px;width: 100%;cursor: inherit;}";
        }elseif ( $type == 'fail' ){
            $display= $style->display;
            $fail= $style->fail;
            $msg = ( $msg == '' ) ? $fail->msg : $msg;
            $disp['msg'] = "<div id='unique-code'><div class='fail-msg'><div style='width: 80%;'>$msg</div><a class='ta-lnk'>Try again</a></div><button type='button' class='btn btn-blocked-unique-code'>$display->blocked_atc_btn_text</button></div>";

            $disp['form'] = "<div id='unique-code'><p class='code-title'>$display->title</p><div class='d-flex justify-content-between unique-form'><input type='text' class='unique-code-input' placeholder='".$display->input_plhol_text. "' name='unique_code' required><button type='button' class='btn btn-unique-code validate_code'>$display->submit_btn_text</button></div><button type='button' class='btn btn-blocked-unique-code'>$display->blocked_atc_btn_text</button></div>";

            $disp['css'] = ".fail-msg{background-color: $fail->bg_color;padding: 10px; display: flex; border:1px solid $fail->border_color; color: $fail->text_color}.ta-lnk{color: $fail->ta_link_color;cursor: pointer;}.code-title{ margin-bottom: 5px; }.unique-code-input{width: 80%;margin-right: 10px;}.code-title{color:$display->title_text_color}.btn-unique-code{color:$display->submit_btn_text_color;background-color:$display->submit_btn_color}.btn-blocked-unique-code{color:$display->blocked_atc_btn_text_color;background-color:$display->blocked_atc_btn_color;border: 1px solid $display->blocked_atc_btn_border_color;margin: 10px 0px;width: 100%;cursor: inherit;}";
        }elseif ( $type == 'success' ){
            $style= $style->success;
            $disp['msg'] = "<div id='unique-code' class='success-msg'>$style->msg</div>";

            $disp['css'] = "#unique-code{margin-bottom: 10px;}.success-msg{background-color: $style->bg_color;padding: 10px; border:1px solid $style->border_color; color: $style->text_color}";
        }

        return $disp;
    }

    public function validateCode(Request  $request){
        try{
            $data = $request->data;
            $code = $data['code'];
            $res = [];
            $domain = $data['domain'];
            $shop = User::where('name', $domain)->first();
            $setting = Setting::select('style')->where('user_id', $shop->id)->first();
            foreach ( $data['ids'] as $key=>$val ){
                $dbcode = Code::select('title', 'generated_codes', 'usage_limit', 'used_limit')->where('user_id', $shop->id)->where('id', $val)->first();

                if( $dbcode ){
                    $checkCodes = json_decode($dbcode['generated_codes']);
                    if( !in_array($code, $checkCodes) ){
                        $res['success'] = false;
                        $res['display'] = $this->getHtml(json_decode($setting->style), 'fail', '');
                    }elseif ( in_array($code, $checkCodes) && $dbcode->is_enable_usage_limit && $dbcode->used_limit >= $dbcode->usage_limit ){
                        $msg = 'Limit reached for this code';
                        $res['success'] = true;
                        $res['display'] = $this->getHtml(json_decode($setting->style), 'fail', $msg);
                        $res['msg'] = 'limit';
                    }else{
                        $ucode = Code::find($val);

                        if( $ucode->is_enable_usage_limit ){
                            $ucode->update(['used_limit' => $ucode->used_limit + 1]);
                            $ucode->save();
                        }

                        $res['success'] = true;
                        $res['display'] = $this->getHtml(json_decode($setting->style), 'success', '');
                        $res['msg'] = '';
                    }
                }

                if( $res['success'] ){
                    break;
                }
            }

            return response::json(['data' => $res], 200);
        }catch ( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }
}
