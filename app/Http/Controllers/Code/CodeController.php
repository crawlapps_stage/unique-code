<?php

namespace App\Http\Controllers\Code;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCode;
use App\Models\Code;
use App\Models\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class CodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request)
    {
        try {
            $shop = Auth::user();
            $code = Code::where('user_id', $shop->id)->get()->toArray();
            return response::json(['data' => $code], 200);
        } catch (\Exception $e) {
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCode $request)
    {
        try {
            $shop = Auth::user();
            $data = $request->data;
            $code =  new Code;
            $start_time = ( $data['start_time'] == '' ) ? date("H:i:s", strtotime($data['start_date'])) : $data['start_time'];
            $start_date = date("Y-m-d", strtotime($data['start_date'])) . ' ' . $start_time;

            $generatedCode = ( $data['is_random'] == 1 ) ? $this->generateCode($data['code_details']) : explode(',', $data['generated_code']);

            if( $data['applies_to'] == 'specific_types' ){
                $apply_data = json_encode($data['specific_types']);
            }elseif ( $data['applies_to'] == 'specific_tags' ){
                $apply_data = json_encode($data['specific_tags']);
            }else{
                $apply_data = $data['applies_to'];
            }
            $code->user_id = $shop->id;
            $code->title = $data['title'];
            $code->is_random = $data['is_random'];
            $code->code_details = json_encode($data['code_details']);
            $code->generated_codes = json_encode($generatedCode);
            $code->applies_to = $data['applies_to'];
            $code->apply_data = $apply_data;
            $code->customer_eligibility = $data['customer_eligibility'];
            $code->is_enable_usage_limit = $data['is_enable_usage_limit'];
            $code->usage_limit = $data['usage_limit'];
            $code->is_set_end_date = $data['is_set_end_date'];
            $code->start_date = $start_date;

            if( $data['is_set_end_date'] ){
                $time = ( $data['end_time'] == '' ) ? date("H:i:s", strtotime($data['end_date'])) : $data['end_time'];
                $date = date("Y-m-d", strtotime($data['end_date'])) . ' ' . $time;
                $code->end_date = $date;
            }else{
                $code->end_date = null;
            }
            $code->save();

            $resources = Resource::where('user_id', $shop->id)->where('code_id', $code->id)->get();
            if( count( $resources ) > 0 ){
                foreach ( $resources as $key=>$val ){
                    $val->delete();
                }
            }
            if( $data['applies_to'] == 'specific_product' || $data['applies_to'] == 'specific_collection' ){
                $at = $data['applies_to'];
                foreach ( $data[$at] as $key=>$val){
                    $resource = new Resource;
                    $resource->user_id = $shop->id;
                    $resource->code_id = $code->id;
                    $resource->resource_id = $val['resource_id'];
                    $resource->title = $val['title'];
                    $resource->handle = $val['handle'];
                    $resource->image = $val['image'];
                    $resource->save();
                }
            }
            $res['msg'] = 'Saved!';
            $res['id'] = $code->id;
            return response::json(['data' => $res], 200);
        } catch (\Exception $e) {
            return response::json(['data' => $e->getMessage()], 422);
        }
    }


//    public function store(CreateCode $request)
//    {
//        try {
//            $shop = Auth::user();
//            $data = $request->data;
//
//            $is_exist = Code::where('user_id', $shop->id)->first();
//            $code =  ( $is_exist ) ? $is_exist : new Code;
//
//            $start_time = ( $data['start_time'] == '' ) ? date("H:i:s", strtotime($data['start_date'])) : $data['start_time'];
//            $start_date = date("Y-m-d", strtotime($data['start_date'])) . ' ' . $start_time;
//
//            $generatedCode = ( $data['is_random'] == 1 ) ? $this->generateCode($data['code_details']) : array($data['title']);
//
//            foreach ( $generatedCode as $key=>$val ){
//                $code = new Code;
//                $code->user_id = $shop->id;
//                $code->title = $val;
//                $code->is_random = $data['is_random'];
//                $code->code_details = json_encode($data['code_details']);
//                $code->applies_to = $data['applies_to'];
//                $code->apply_data = ( $data['applies_to'] != 'all_product') ?  json_encode($data[$data['applies_to']]) : '';
//                $code->customer_eligible = $data['customer_eligible'];
//                $code->is_enable_usage_limit = $data['is_enable_usage_limit'];
//                $code->usage_limit = $data['usage_limit'];
//                $code->is_set_end_date = $data['is_set_end_date'];
//                $code->start_date = $start_date;
//
//                if( $data['is_set_end_date'] ){
//                    $time = ( $data['end_time'] == '' ) ? date("H:i:s", strtotime($data['end_date'])) : $data['end_time'];
//                    $date = date("Y-m-d", strtotime($data['end_date'])) . ' ' . $time;
//                    $code->end_date = $date;
//                }else{
//                    $code->end_date = null;
//                }
//                $code->save();
//            }
//            $code->save();
//            return response::json(['data' => 'Saved!'], 200);
//        } catch (\Exception $e) {
//            return response::json(['data' => $e->getMessage()], 422);
//        }
//    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Code $code)
    {
        try{
            $data['id'] = $code->id;
            $data['title'] = $code->title;
            $data['is_random'] = 2;
            $data['code_details'] = json_decode($code->code_details);
            $data['generated_code'] = json_decode($code->generated_codes);
            $data['applies_to'] = $code->applies_to;
            $data['customer_eligibility'] = $code->customer_eligibility;
            $data['is_enable_usage_limit'] = $code->is_enable_usage_limit;
            $data['usage_limit'] = $code->usage_limit;
            $data['start_date'] = $code->start_date;
            $data['start_time'] = date("H:i", strtotime($code->start_date));
            $data['is_set_end_date'] = $code->is_set_end_date;
            $data['end_date'] = $code->end_date;
            $data['end_time'] = ($code->end_time) ? date("H:i", strtotime($code->end_date)) : '';

            $data['specific_product'] = [];
            $data['specific_collection'] = [];
            $data['specific_types'] = [''];
            $data['specific_tags'] = [''];
            if ( $code->applies_to == 'specific_product' ||  $code->applies_to == 'specific_collection'){
                $resource = Resource::select('title', 'handle', 'resource_id', 'image')->where('code_id', $code->id)->where('user_id', $code->user_id)->get()->toArray();
                $data[ $code->applies_to] = $resource;
            }else{
                $data[ $code->applies_to] = json_decode( $code->apply_data );
            }

            return response::json(['data' => $data], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }
//    public function edit(Code $code)
//    {
//        try{
//            $data['id'] = $code->id;
//            $data['title'] = $code->title;
//            $data['is_random'] = 2;
//            $data['code_details'] = json_decode($code->code_details);
//            $data['applies_to'] = $code->applies_to;
//            $data['customer_eligible'] = $code->customer_eligible;
//            $data['is_enable_usage_limit'] = $code->is_enable_usage_limit;
//            $data['usage_limit'] = $code->usage_limit;
//            $data['start_date'] = $code->start_date;
//            $data['start_time'] = date("H:i", strtotime($code->start_date));
//            $data['is_set_end_date'] = $code->is_set_end_date;
//            $data['end_date'] = $code->end_date;
//            $data['end_time'] = ($code->end_time) ? date("H:i", strtotime($code->end_date)) : '';
//
//            $data['specific_product'] = [];
//            $data['specific_collection'] = [];
//            $data['specific_types'] = [''];
//            $data['specific_tags'] = [''];
//            $data['status'] = $code->status;
//            $data[$code->applies_to] = json_decode($code->apply_data);
//
//            return response::json(['data' => $data], 200);
//        }catch( \Exception $e ){
//            return response::json(['data' => $e->getMessage()], 422);
//        }
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateCode $request,Code $code)
    {
        try{
            $shop = Auth::user();
            $data = $request->data;
            $start_time = ( $data['start_time'] == '' ) ? date("H:i:s", strtotime($data['start_date'])) : $data['start_time'];
            $start_date = date("Y-m-d", strtotime($data['start_date'])) . ' ' . $start_time;

            if( $data['applies_to'] == 'specific_types' ){
                $apply_data = json_encode($data['specific_types']);
            }elseif ( $data['applies_to'] == 'specific_tags' ){
                $apply_data = json_encode($data['specific_tags']);
            }else{
                $apply_data = $data['applies_to'];
            }

            $code->title = $data['title'];
            $code->applies_to = $data['applies_to'];
            $code->generated_codes = $data['generated_code'];;
            $code->apply_data = $apply_data;
            $code->customer_eligibility = $data['customer_eligibility'];
            $code->is_enable_usage_limit = $data['is_enable_usage_limit'];
            $code->usage_limit = $data['usage_limit'];
            $code->is_set_end_date = $data['is_set_end_date'];
            $code->start_date = $start_date;

            if( $data['is_set_end_date'] ){
                $time = ( $data['end_time'] == '' ) ? date("H:i:s", strtotime($data['end_date'])) : $data['end_time'];
                $date = date("Y-m-d", strtotime($data['end_date'])) . ' ' . $time;
                $code->end_date = $date;
            }else{
                $code->end_date = null;
            }
            $code->save();

            $resources = Resource::where('user_id', $shop->id)->where('code_id', $code->id)->get();
            if( count( $resources ) > 0 ){
                foreach ( $resources as $key=>$val ){
                    $val->delete();
                }
            }
            if( $data['applies_to'] == 'specific_product' || $data['applies_to'] == 'specific_collection' ){
                $at = $data['applies_to'];
                foreach ( $data[$at] as $key=>$val){
                    $resource = new Resource;
                    $resource->user_id = $shop->id;
                    $resource->code_id = $code->id;
                    $resource->resource_id = $val['resource_id'];
                    $resource->title = $val['title'];
                    $resource->handle = $val['handle'];
                    $resource->image = $val['image'];
                    $resource->save();
                }
            }
            $res['msg'] = 'Saved!';
            $res['id'] = $code->id;
            return response::json(['data' => $res], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Code $code)
    {
        try{
            $code->delete();
            return response::json(['data' => 'Deleted!'], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    public function generateCode($data){
        try{
            $code = [];
            for( $i=0; $i < $data['total_codes']; $i++ ){
                $lc = range('a', 'z');
                $uc = range('A', 'Z');
                $digits =  range(0,9);

                shuffle($lc);
                shuffle($uc);
                shuffle($digits);

                if( $data['allowed_character'] == 'both' ){
                    $DIG_UC = array_merge( $uc, $digits);
                    $ALL = array_merge( $DIG_UC, $lc);
                }elseif ( $data['allowed_character'] == 'latters' ){
                    $ALL = array_merge( $uc, $lc);
                }elseif ( $data['allowed_character'] == 'numbers' ){
                    if( count($digits) < $data['code_length'] ){
                        $d = range(0,$data['code_length']);
                    }
                    $ALL = $digits;
                }
                shuffle($ALL);
                $prelen = ( $data['prefix'] ) ? strlen($data['prefix']) : 0;
                $suflen = ( $data['suffix'] ) ? strlen($data['suffix']) : 0;
                $presuflen = $prelen + $suflen;
                $otherlen = ( $data['code_length'] > $presuflen ) ? $data['code_length'] - $presuflen: $data['code_length'];

                if( $data['is_include_saperater'] ){
                    $code[] = $data['prefix'] . '-' . substr(implode('', array_slice($ALL,0,$otherlen - 1)), 0, $otherlen - 1) . $data['suffix'];
                }else{
                    $code[] = $data['prefix'] . substr(implode('', array_slice($ALL,5,$otherlen)), 0, $otherlen) . $data['suffix'];
                }
            }
            return $code;
        }catch( \Exception $e){
            dd($e);
        }
    }

    public function changeStatus(Request $request){
        try{
            $shop = Auth::user();
            $code = Code::where('user_id', $shop->id)->where('id', $request->id)->first();
            $code->status = ( $code->status ) ? 0 : 1;
            $code->save();
            return response::json(['data' => 'Saved!'], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }
}
