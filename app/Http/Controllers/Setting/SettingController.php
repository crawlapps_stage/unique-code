<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index( ){
        try{
            $shop = \Auth::user();
            $entity = Setting::where('user_id', $shop->id)->first();
            return \Response::json([
                'settings' => json_decode(@$entity->style),

            ], 200);
        }catch( \Exception $e ){
            return response(['data' => $e->getMessage()], 422);
        }
    }
    public function store( Request $request ){
        try{
            $shop = \Auth::user();
            $entity = Setting::where('user_id', $shop->id)->first();
            $entity = ( $entity ) ? $entity : new Setting;
            $entity->user_id = $shop->id;
            $entity->style = json_encode($request->data);
            $entity->save();

            return response(['message' => 'Saved!'], 200);
        }catch( \Exception $e ){
            return response(['message' => $e->getMessage()], 422);
        }
    }
}
