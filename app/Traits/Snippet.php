<?php

namespace App\Traits;

use App\Exceptions\ApiOperationFailedException;
use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait AddSnippet
 * @package App\Traits
 */
trait Snippet
{
    /**
     * @param string $file
     *
     * @return bool
     */
    public function addSnippet($domain)
    {
        \Log::info('--------------------- add snippet --------------------');
        $type = 'add';
        $parameter['role'] = 'main';
        $shop = User::where('name', $domain)->first();
        $result = $shop->api()->rest('GET', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/themes.json',$parameter);

        \Log::info(json_encode($result));
        $theme_id = $result['body']->container['themes'][0]['id'];
        \Log::info('Theme id :: ' . $theme_id);
        if($type == 'add') {
            $value = <<<EOF
        <script id="crawlapps-uniquecode" type="application/json">
            {
                "shop": {
                    "domain": "{{ shop.domain }}",
                    "permanent_domain": "{{ shop.permanent_domain }}",
                    "url": "{{ shop.url }}",
                    "secure_url": "{{ shop.secure_url }}",
                    "money_format": {{ shop.money_format | json }},
                    "currency": {{ shop.currency | json }}
                },
                "customer": {
                    "id": {{ customer.id | json }},
                    "tags": {{ customer.tags | json }}
                },
                "cart": {{ cart | json }},
                "template": "{{ template | split: "." | first }}",
                "product": {{ product | json }},
                "collection": {{ product.collections | json }}
            }
        </script>
EOF;
        }
        $parameter['asset']['key'] = 'snippets/crawlapps_uniquecode.liquid';
        $parameter['asset']['value'] = $value;
        $asset = $shop->api()->rest('PUT', 'admin/themes/'.$theme_id . '/assets.json',$parameter);

        $this->updateThemeLiquid($theme_id, 'crawlapps_uniquecode', $shop);
    }

    /**
     * @param $theme_id
     * @param $snippet_name
     * @param $shop
     */
    public function updateThemeLiquid($theme_id, $snippet_name, $shop)
    {
        try {
            \Log::info('-----------------------updateThemeLiquid-----------------------');
            $asset = $shop->api()->rest('GET', 'admin/themes/'.$theme_id.'/assets.json',
                ["asset[key]" => 'layout/theme.liquid']);
            if (@$asset['body']->container['asset']) {
                $asset = $asset['body']->container['asset']['value'];

                if (!strpos($asset, "{% include '$snippet_name' %}")) {
                    $asset = str_replace('</head>', "{% include '$snippet_name' %}</head>", $asset);
                }

                $parameter['asset']['key'] = 'layout/theme.liquid';
                $parameter['asset']['value'] = $asset;
                $result = $shop->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);
                \Log::info(json_encode($result));
            }
        } catch (\Exception $e) {
            \Log::info('------------ERROR :: updateThemeLiquid--------------');
            \Log::info(json_encode($e));
        }

    }
}

