<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shopify'])->name('home');

Route::group(['middleware' => ['auth.shopify']], function () {
    Route::resource('code', 'Code\CodeController');
    Route::get('change-status', 'Code\CodeController@changeStatus');
    Route::get('setting', 'Setting\SettingController@index');
    Route::post('setting', 'Setting\SettingController@store');
    Route::get('test', 'Test\TestController@index');
});

Route::get('flush', function(){
    request()->session()->flush();
});
